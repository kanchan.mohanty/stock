package com.ing.StockPurchase.model;

public class StockPrice {
	
	private double totalPrice;
	private double brokerageFee;
	private double stockPrice;
	
	
	public double getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public double getBrokerageFee() {
		return brokerageFee;
	}
	public void setBrokerageFee(double brokerageFee) {
		this.brokerageFee = brokerageFee;
	}
	

}
