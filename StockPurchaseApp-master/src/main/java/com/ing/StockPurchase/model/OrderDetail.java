package com.ing.StockPurchase.model;

import java.util.Date;

public class OrderDetail {
	
	private Date timeOfTrade;
	private String stockName;
	private Double stockPrice;
	private int purchasedVolume;
	private Double totalPurchasePrice;
	private Double fees;
	private Double totalFee;
	
	public Date getTimeOfTrade() {
		return timeOfTrade;
	}
	public void setTimeOfTrade(Date timeOfTrade) {
		this.timeOfTrade = timeOfTrade;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public Double getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}
	public int getPurchasedVolume() {
		return purchasedVolume;
	}
	public void setPurchasedVolume(int purchasedVolume) {
		this.purchasedVolume = purchasedVolume;
	}
	public Double getTotalPurchasePrice() {
		return totalPurchasePrice;
	}
	public void setTotalPurchasePrice(Double totalPurchasePrice) {
		this.totalPurchasePrice = totalPurchasePrice;
	}
	public Double getFees() {
		return fees;
	}
	public void setFees(Double fees) {
		this.fees = fees;
	}
	public Double getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}
	
}
