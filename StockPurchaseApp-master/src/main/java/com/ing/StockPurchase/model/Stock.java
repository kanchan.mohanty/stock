package com.ing.StockPurchase.model;

public class Stock {
     private double price;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
