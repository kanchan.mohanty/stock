package com.ing.StockPurchase.model;

public class StockDetails {

	private int id;
	private String stockName;
	private Double stockPrice;
	private Double updatedPrice;

	public Double getUpdatedPrice() {
		return updatedPrice;
	}

	public void setUpdatedPrice(Double updatedPrice) {
		this.updatedPrice = updatedPrice;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
