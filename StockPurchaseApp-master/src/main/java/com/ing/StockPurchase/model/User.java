package com.ing.StockPurchase.model;

import java.util.List;

public class User {
	
	private List<UserDetails> userData;

	public List<UserDetails> getUserData() {
		return userData;
	}

	public void setUserData(List<UserDetails> userData) {
		this.userData = userData;
	}

}
