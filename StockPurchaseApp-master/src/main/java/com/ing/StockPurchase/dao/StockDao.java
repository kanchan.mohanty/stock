package com.ing.StockPurchase.dao;

import java.util.List;

import com.ing.StockPurchase.model.OrderDetail;
import com.ing.StockPurchase.model.OrderSummary;
import com.ing.StockPurchase.model.Stock;
import com.ing.StockPurchase.model.StockDetails;
import com.ing.StockPurchase.model.UserDetails;

public interface StockDao {

	public List<UserDetails> getUserDetails();

	public List<StockDetails> getStockDetails();

	public StockDetails getStockDetailById(int id);
	
	public StockDetails addStockDetail(StockDetails stockdetails);

	public StockDetails addStockDetail2(String stockName, Double stockPrice);
	
	//public void addOrder(String name,double  price,int  qty,String username);

	public void addOrder(OrderSummary ordersummary);
	
	public Stock getStockPriceById(int stockId);

	public StockDetails getStockDetailUpdated(int id);
	
	public List<OrderDetail> getOrderSummary(int userId);

}
