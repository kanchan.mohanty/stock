package com.ing.StockPurchase.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ing.StockPurchase.model.OrderDetail;
import com.ing.StockPurchase.model.OrderSummary;
import com.ing.StockPurchase.model.Stock;
import com.ing.StockPurchase.model.StockDetails;
import com.ing.StockPurchase.model.UserDetails;

@Repository
public class StockDaoImpl implements StockDao {
	@Autowired
	public JdbcTemplate template;

	public Stock getStockPriceById(int stockId) {
		
		String query2 = "SELECT stock_price FROM stock_details where id= ? ";
		
		//return (double) template.queryForObject(query2, new Object[] {stockId}, new StockPriceRowMapper() );
		return template.queryForObject(query2, new Object[] {stockId}, new StockPriceRowMapper());
		
	}
	
	@Override
	public List<UserDetails> getUserDetails() {

		String query = "select *  from user_details";
		
		// return query;
		return template.query(query, new UserDetailsRowMapper());
	}

	public class UserDetailsRowMapper implements RowMapper<UserDetails> {

		@Override
		public UserDetails mapRow(ResultSet resultSet, int arg1) throws SQLException {

			UserDetails userDetails = new UserDetails();

			userDetails.setUserId(resultSet.getInt("user_id"));
			userDetails.setUserName(resultSet.getString("user_name"));

			return userDetails;
		}

	}

	@Override
	public List<StockDetails> getStockDetails() {
		String query1 = "SELECT id, stock_name,stock_price FROM stock_details ";
		
		return template.query(query1, new StockDetailsRowMapper());
	}
	
	public class StockDetailsRowMapper implements RowMapper<StockDetails>{

		@Override
		public StockDetails mapRow(ResultSet resultset1, int arg1) throws SQLException {
			
			StockDetails stockdetails = new StockDetails();
			stockdetails.setId(resultset1.getInt("id"));
			stockdetails.setStockName(resultset1.getString("stock_name"));
			stockdetails.setStockPrice(resultset1.getDouble("stock_price"));
			
			return stockdetails;
		}
		
	}

	@Override
	public StockDetails getStockDetailById(int id) {
		String query1 = "SELECT id, stock_name,stock_price FROM stock_details where id= ? ";
		
		return template.queryForObject(query1, new Object[] {id}, new StockDetailsRowMapper() );
		
	}
	
	@Override
	public StockDetails addStockDetail(StockDetails stockdetails) {

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		java.sql.Date requestedOn;
		try {
			requestedOn = new java.sql.Date(formatter.parse(LocalDateTime.now().toString()).getTime());
			System.out.println("parsing");
		} catch (ParseException e) {
			System.out.println("exception");
			requestedOn = new java.sql.Date(System.currentTimeMillis());
		}
		Double price = stockdetails.getStockPrice();
		System.out.println(price);

		String sql = "insert into stock_details(stock_name,stock_price) values(?,?)";
		template.update(sql, new Object[] { stockdetails.getStockName(),
				stockdetails.getStockPrice() });
		return stockdetails;

	}

	@Override
	public StockDetails addStockDetail2(String stockName, Double stockPrice) {
		
		String query2 = "SELECT id, stock_name,stock_price FROM stock_details where id= ? ";
		
		
		return null;
	}
/*	
	public void addOrder(String name,double  price,int  qty,String username) {
		//LOGGER.info("::::Inside addStock method of StockController:::");
		double total=0.0;
		Timestamp createddate = new Timestamp(System.currentTimeMillis());
		String sql = "insert into order_summary(stock_name,stock_price,quantity,total,created_date,created_user) values(?,?,?,?,?,?)";
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		java.sql.Date requestedOn;
		try {
			requestedOn = new java.sql.Date(formatter.parse(LocalDateTime.now().toString()).getTime());
			System.out.println("parsing");
		} catch (ParseException e) {
			System.out.println("exception");
			requestedOn = new java.sql.Date(System.currentTimeMillis());
		}
		
		System.out.println("Date:::" +requestedOn);
		
		if( qty < 500 ){
			total = price*qty + 0.10;
			template.update(sql, new Object[] { name, price, qty, total , createddate, username });
		} else {
			total = price*qty + 0.15;
			template.update(sql, new Object[] { name, price, qty, total, createddate, username });
		}
	}*/

	@Override
	public void addOrder(OrderSummary ordersummary) {
		
		Timestamp createddate = new Timestamp(System.currentTimeMillis());
		String sql = "insert into order_summary(stock_name,stock_price,quantity,total,created_date,user_id,br_price) values(?,?,?,?,?,?,?)";
		
		template.update(sql, new Object[] { ordersummary.getStockName(),ordersummary.getPrice() , ordersummary.getQty(), ordersummary.getTotalPrice(), createddate, ordersummary.getUserId(),ordersummary.getBrokerageFee()});
		
	}
	
	public class StockPriceRowMapper implements RowMapper<Stock>{

		@Override
		public Stock mapRow(ResultSet resultset1, int arg1) throws SQLException {
			
			Stock stock = new Stock();
			//stockdetails.setId(resultset1.getInt("id"));
			//stockdetails.setStockName(resultset1.getString("stock_name"));
			stock.setPrice(resultset1.getDouble("stock_price"));
			
			return stock;
		}
		
	}

	@Override
	public StockDetails getStockDetailUpdated(int id) {
	String query1 = "SELECT id, stock_name,stock_updated_price FROM stock_details where id= ? ";
		
		return template.queryForObject(query1, new Object[] {id}, new StockDetailsUpdatedRowMapper() );
	}
	
	public class StockDetailsUpdatedRowMapper implements RowMapper<StockDetails>{

		@Override
		public StockDetails mapRow(ResultSet resultset1, int arg1) throws SQLException {
			
			StockDetails stockdetails = new StockDetails();
			stockdetails.setId(resultset1.getInt("id"));
			stockdetails.setStockName(resultset1.getString("stock_name"));
			//stockdetails.setStockPrice(resultset1.getDouble("stock_price"));
			stockdetails.setUpdatedPrice(resultset1.getDouble("stock_updated_price"));
			return stockdetails;
		}
	}
	
	@Override
	public List<OrderDetail> getOrderSummary(int userId) {
		String query4 = "SELECT created_date, stock_name,stock_price,quantity,total,br_price FROM order_summary where user_id= ? ";
		
		return template.query(query4, new Object[] {userId}, new OrderDetailsRowMapper() );
		  //return template.queryForList(query4, new Object[] {userId}, new OrderDetailsRowMapper() );
	}
	
	public class OrderDetailsRowMapper implements RowMapper<OrderDetail>{

		@Override
		public OrderDetail mapRow(ResultSet resultset1, int arg1) throws SQLException {
			OrderDetail orderDetail = new OrderDetail();
			orderDetail.setTimeOfTrade(resultset1.getDate("created_date"));
			orderDetail.setStockName(resultset1.getString("stock_name"));
			orderDetail.setStockPrice(resultset1.getDouble("stock_price"));
			orderDetail.setPurchasedVolume(resultset1.getInt("quantity"));
			orderDetail.setTotalPurchasePrice(resultset1.getDouble("total"));
			orderDetail.setFees(resultset1.getDouble("br_price"));
			orderDetail.setTotalFee(resultset1.getDouble("br_price"));
			
			return orderDetail;
		}
		
	}

}
