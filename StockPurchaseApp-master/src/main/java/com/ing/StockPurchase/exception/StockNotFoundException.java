package com.ing.StockPurchase.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(value="HttpStatus.NOT_FOUND",reason="This stock is not present in the system")
public class StockNotFoundException extends Exception {
	
	private static final long serialVersionUID = 100L;

}
