package com.ing.StockPurchase.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ing.StockPurchase.model.OrderDetail;
import com.ing.StockPurchase.model.OrderSummary;
import com.ing.StockPurchase.model.Stock;
import com.ing.StockPurchase.model.StockDetails;
import com.ing.StockPurchase.model.StockPrice;
import com.ing.StockPurchase.model.UserDetails;
import com.ing.StockPurchase.service.StockService;

@RestController
@CrossOrigin("*")
@RequestMapping("/StockPurchase")
public class StockController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

	@Autowired
	StockService stockService;
	
	@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET, produces = "application/json")
	public List<UserDetails> getUserDetails() {
	LOGGER.info("::::Inside getUserDetails method of StockController:::");
	
		return stockService.getUserDetails();
	}
	
	/*@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET, produces = "application/json")
	public User getUserDetails() {
	LOGGER.info("::::Inside getUserDetails method of StockController:::");
	
		User user = new User();
		user.setUserData(stockService.getUserDetails());
		return user;

	}*/
	
	@RequestMapping(value = "/getStockDetails", method = RequestMethod.GET, produces = "application/json")
	public List<StockDetails> getStockDetails() {
	LOGGER.info("::::Inside getStockDetails method of StockController:::");

		return stockService.getStockDetails();

	}
	
	@RequestMapping(value = "/getStockDetail/{id}", method = RequestMethod.GET, produces = "application/json")
	public StockDetails getStockDetailById(@PathVariable("id") int id) {
	LOGGER.info("::::Inside getStockDetails1 method of StockController:::");
	
		return stockService.getStockDetailById(id);

	}
	
	@RequestMapping(value = "/addStockDetail", method = RequestMethod.POST, produces = "application/json")
	public StockDetails addStock(@RequestBody StockDetails stockdetails) {
	LOGGER.info("::::Inside addStock method of StockController:::");

		return stockService.addStockDetails(stockdetails);

	}
	
	/*@RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = "application/json")
	public void addOrder(@RequestParam("name") String name,@RequestParam("price") double  price,@RequestParam("qty") int  qty, @RequestParam("username") String username) {
	LOGGER.info("::::Inside addOrder method of StockController:::");

		stockService.addOrder(name,price,qty,username);

	}*/
	
	@RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = "application/json")
	public void addOrderSummary(@RequestBody OrderSummary ordersummary) {
	LOGGER.info("::::Inside addOrder method of StockController:::");

		stockService.addOrder(ordersummary);

	}
	
/*	@RequestMapping(value = "/calc", method = RequestMethod.POST)
	public StockPrice calculatePrice(@RequestParam("userId") String userId,@RequestParam("stockId") String stockId,@RequestParam("stockName") String stockName,@RequestParam("price") double  price,@RequestParam("qty") int  qty  ) {
	LOGGER.info("::::Inside addStock method of StockController:::");
	
		//return stockService.calculateTotalPrice(name, price, qty);
	//System.out.println("StockName" +name+"stockprice" + price);
	double total=0.0;
	double fee = 0.0;
	double brFee = 0.0;
	StockPrice stockPrice = new StockPrice();
	if( qty< 500){
		 //total = price*qty;
		 fee = 0.10;
		 brFee = price*qty*fee;
		 total = price*qty + brFee;
		 
		 stockPrice.setBrokerageFee(brFee);
		 stockPrice.setTotalPrice(total);
		//return name+" "+price*qty
	} else{
		//total = price*qty + 0.15;
		 fee = 0.15;
		 brFee = price*qty*fee;
		 total = price*qty + brFee;
		 stockPrice.setBrokerageFee(brFee);
		 stockPrice.setTotalPrice(total);
	}

	return stockPrice;
		//return name+" "+price*qty+" "+total;*/

	
	@RequestMapping(value = "/calc", method = RequestMethod.POST)
	public StockPrice orderSummary(@RequestBody OrderSummary ordersummary){
		LOGGER.info("::::Inside addStock method of StockController:::");
		double total=0.0;
		double fee = 0.0;
		double brFee = 0.0;
		double updatedPrice = 0.0;
		Stock stock = new Stock();
		stock = stockService.getStockPriceById(ordersummary.getStockId());
		updatedPrice = stock.getPrice();
		System.out.println(updatedPrice);
		

		StockPrice stockPrice = new StockPrice();
		
		
		if( ordersummary.getQty()< 500){
			 //total = price*qty;
			 fee = 0.10;
			 brFee = (ordersummary.getPrice()*ordersummary.getQty()*fee)/100;
			 total = ordersummary.getPrice()*ordersummary.getQty()+ brFee;;
			 
			 stockPrice.setBrokerageFee(brFee);
			 stockPrice.setTotalPrice(total);
			 stockPrice.setStockPrice(updatedPrice);
			//return name+" "+price*qty
		} else{
			
			 fee = 0.15;
			 brFee = (ordersummary.getPrice()*ordersummary.getQty()*fee)/100;
			 total = ordersummary.getPrice()*ordersummary.getQty()+ brFee;
			 
			 stockPrice.setBrokerageFee(brFee);
			 stockPrice.setTotalPrice(total);
			 stockPrice.setStockPrice(updatedPrice);
			
		}
		
		
			return stockPrice;
			}
	
	@RequestMapping(value = "/getOrderSummary/{userId}", method = RequestMethod.GET, produces = "application/json")
	public  List<OrderDetail> getOrderSummary(@PathVariable("userId") int userId) {
		
	LOGGER.info("::::Inside getStockDetails1 method of StockController:::");
	
		return stockService.getOrderSummary(userId);

	}
	
	
	
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String getHello() {

		return "hello world";

	}
	


	
	

}
