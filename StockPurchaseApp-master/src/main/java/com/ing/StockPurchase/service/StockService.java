package com.ing.StockPurchase.service;

import java.util.List;

import com.ing.StockPurchase.model.OrderDetail;
import com.ing.StockPurchase.model.OrderSummary;
import com.ing.StockPurchase.model.Stock;
import com.ing.StockPurchase.model.StockDetails;
import com.ing.StockPurchase.model.UserDetails;

public interface StockService {

	public List<UserDetails> getUserDetails();

	public List<StockDetails> getStockDetails();

	public StockDetails getStockDetailById(int id);
	
	public StockDetails addStockDetails(StockDetails stockdetails);

	public StockDetails addStockDetails2(String stockName, Double stockPrice);
	
	//public void addOrder(String name,double  price,int  qty,String username);
	
	public String calculateTotalPrice(String name,double  price,int  qty);

	public void addOrder(OrderSummary ordersummary);

	public Stock getStockPriceById(int stockId);

	public StockDetails getStockDetailUpdated(int id);

	public List<OrderDetail> getOrderSummary(int userId);

}
