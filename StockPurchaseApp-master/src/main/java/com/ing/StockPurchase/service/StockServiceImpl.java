package com.ing.StockPurchase.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.StockPurchase.dao.StockDao;
import com.ing.StockPurchase.model.OrderDetail;
import com.ing.StockPurchase.model.OrderSummary;
import com.ing.StockPurchase.model.Stock;
import com.ing.StockPurchase.model.StockDetails;
import com.ing.StockPurchase.model.UserDetails;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	StockDao stockDao;

	@Override
	public List<UserDetails> getUserDetails() {

		return stockDao.getUserDetails();
	}

	@Override
	public List<StockDetails> getStockDetails() {

		return stockDao.getStockDetails();
	}

	@Override
	public StockDetails getStockDetailById(int id) {

		return stockDao.getStockDetailById(id);
	}

	@Override
	public StockDetails addStockDetails(StockDetails stockdetails) {

		return stockDao.addStockDetail(stockdetails);
	}

	@Override
	public StockDetails addStockDetails2(String stockName, Double stockPrice) {
		
		return stockDao.addStockDetail2(stockName,stockPrice);
	}
	
	/*@Override
	public void addOrder(String name,double  price,int  qty,String username){
		stockDao.addOrder(name, price, qty, username);
	}*/
	
	public String calculateTotalPrice(String name,double  price,int  qty) {
	//LOGGER.info("::::Inside addStock method of StockController:::");
		double total=0.0;
		if( qty< 500){
			 total = price*qty + 0.10;
		} else{
			total = price*qty + 0.15;
		}

			return name+" "+price*qty+" "+total;

		}

	@Override
	public void addOrder(OrderSummary ordersummary) {
		stockDao.addOrder(ordersummary);
		
	}
	
	public Stock getStockPriceById(int stockId){
		return stockDao.getStockPriceById(stockId);
	}

	@Override
	public StockDetails getStockDetailUpdated(int id) {
		
		return stockDao.getStockDetailUpdated(id);
	}
	
	public List<OrderDetail> getOrderSummary(int userId){
		return stockDao.getOrderSummary(userId);
	}
		
}
